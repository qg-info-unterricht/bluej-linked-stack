/**
 * Abstrakte Basisklasse für Stacks
 * 
 * @author Rainer Helfrich
 * @version Oktober 2020
 */
public abstract class Stack<T>
{
    /**
     * Gibt zurück, ob der Stack leer ist
     * @return true, wenn der Stack leer ist; false sonst
     */
    public abstract boolean isEmpty();
    
    /**
     * Legt ein neues Element auf den Stack
     * @param x Das neue Element
     */
    public abstract void push(T x);
    
    /**
     * Entfernt das oberste Element vom Stack (falls der Stack nicht leer ist) und gibt es zurück
     * @return Das bisherige oberste Element
     */
    public abstract T pop();
    
    /**
     * Gibt das oberste Element des Stacks zurück (falls der Stack nicht leer ist)
     * @return Das oberste Element
     */
    public abstract T top();
}
