 

/**
 * Klasse Node
 * 
 * @author Rainer Helfrich
 * @version Oktober 2020
 */
public class Node<T>
{
    /**
     * Der Datenwert des Listenknotens
     */
    public T data;
    
    /**
     * Der Nachfolger des Listenknotens
     */
    public Node<T> next;

    /**
     * Erzeugt einen neuen Listenknoten
     * 
     * @param daten Der Datenwert des Knotens
     * @param nachfolger Der Nachfolger des Knotens
     */
    public Node(T daten, Node<T> nachfolger)
    {
        this.data = daten;
        this.next = nachfolger;
    }
}
